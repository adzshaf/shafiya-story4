from django.shortcuts import render
from datetime import datetime

waktu = datetime.now()

# Create your views here.
def home(request):
    return render(request,'page/index.html', {'current_date':waktu})

def index(request):
    return render(request, 'page/index.html', {'current_date':waktu})

def blog(request):
    return render(request,'page/blog.html', {'current_date':waktu})

def signup(request):
    return render(request,'page/signup.html', {'current_date':waktu})