from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^index/$', views.index, name='index'),
    url(r'^blog/$', views.blog, name='blog'),
    url(r'^signup/$', views.signup, name='signup'),
]